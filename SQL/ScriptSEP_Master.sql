USE [master]
GO
/****** Object:  Database [SEP_Master]    Script Date: 05/23/2013 16:21:17 ******/
CREATE DATABASE [SEP_Master] ON  PRIMARY 
( NAME = N'SEP_MASTER', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SEPSERVER\MSSQL\DATA\SEP_Master.mdf' , SIZE = 3361216KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'SEP_MASTER_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL10_50.SEPSERVER\MSSQL\DATA\SEP_Master_1.ldf' , SIZE = 4211392KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [SEP_Master] SET COMPATIBILITY_LEVEL = 80
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [SEP_Master].[dbo].[sp_fulltext_database] @action = 'disable'
end
GO
ALTER DATABASE [SEP_Master] SET ANSI_NULL_DEFAULT OFF
GO
ALTER DATABASE [SEP_Master] SET ANSI_NULLS OFF
GO
ALTER DATABASE [SEP_Master] SET ANSI_PADDING OFF
GO
ALTER DATABASE [SEP_Master] SET ANSI_WARNINGS OFF
GO
ALTER DATABASE [SEP_Master] SET ARITHABORT OFF
GO
ALTER DATABASE [SEP_Master] SET AUTO_CLOSE OFF
GO
ALTER DATABASE [SEP_Master] SET AUTO_CREATE_STATISTICS ON
GO
ALTER DATABASE [SEP_Master] SET AUTO_SHRINK OFF
GO
ALTER DATABASE [SEP_Master] SET AUTO_UPDATE_STATISTICS ON
GO
ALTER DATABASE [SEP_Master] SET CURSOR_CLOSE_ON_COMMIT OFF
GO
ALTER DATABASE [SEP_Master] SET CURSOR_DEFAULT  GLOBAL
GO
ALTER DATABASE [SEP_Master] SET CONCAT_NULL_YIELDS_NULL OFF
GO
ALTER DATABASE [SEP_Master] SET NUMERIC_ROUNDABORT OFF
GO
ALTER DATABASE [SEP_Master] SET QUOTED_IDENTIFIER OFF
GO
ALTER DATABASE [SEP_Master] SET RECURSIVE_TRIGGERS OFF
GO
ALTER DATABASE [SEP_Master] SET  DISABLE_BROKER
GO
ALTER DATABASE [SEP_Master] SET AUTO_UPDATE_STATISTICS_ASYNC OFF
GO
ALTER DATABASE [SEP_Master] SET DATE_CORRELATION_OPTIMIZATION OFF
GO
ALTER DATABASE [SEP_Master] SET TRUSTWORTHY OFF
GO
ALTER DATABASE [SEP_Master] SET ALLOW_SNAPSHOT_ISOLATION OFF
GO
ALTER DATABASE [SEP_Master] SET PARAMETERIZATION SIMPLE
GO
ALTER DATABASE [SEP_Master] SET READ_COMMITTED_SNAPSHOT OFF
GO
ALTER DATABASE [SEP_Master] SET HONOR_BROKER_PRIORITY OFF
GO
ALTER DATABASE [SEP_Master] SET  READ_WRITE
GO
ALTER DATABASE [SEP_Master] SET RECOVERY FULL
GO
ALTER DATABASE [SEP_Master] SET  MULTI_USER
GO
ALTER DATABASE [SEP_Master] SET PAGE_VERIFY CHECKSUM
GO
ALTER DATABASE [SEP_Master] SET DB_CHAINING OFF
GO
EXEC sys.sp_db_vardecimal_storage_format N'SEP_Master', N'ON'
GO
USE [SEP_Master]
GO
/****** Object:  User [vibanez]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [vibanez] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [usr911db]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [usr911db] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [upepe\vibanez]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [upepe\vibanez] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UPEPE\mdiaz]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [UPEPE\mdiaz] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UPEPE\jvillalobos]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [UPEPE\jvillalobos] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UPEPE\jrodea]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [UPEPE\jrodea] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UPEPE\ggarcia]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [UPEPE\ggarcia] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UPEPE\ebarillas]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [UPEPE\ebarillas] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UPEPE\arivera]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [UPEPE\arivera] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [UPEPE\anmichaca]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [UPEPE\anmichaca] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [sesistemase]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [sesistemase] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [saSeguridad]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [saSeguridad] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [nugasys]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [nugasys] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [maru]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [maru] FOR LOGIN [maru] WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [leonor]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [leonor] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[db_owner]
GO
/****** Object:  User [gvazquez]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [gvazquez] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [GLOPEZC]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [GLOPEZC] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[db_owner]
GO
/****** Object:  User [genDBF911]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [genDBF911] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [EBarillas]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [EBarillas] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [dboCenni]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [dboCenni] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [dboCCT]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [dboCCT] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  User [db911test]    Script Date: 05/23/2013 16:21:17 ******/
CREATE USER [db911test] WITHOUT LOGIN WITH DEFAULT_SCHEMA=[dbo]
GO
/****** Object:  Table [dbo].[ZonaTemp]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ZonaTemp](
	[id_zona] [int] NOT NULL,
	[id_tipoeducacion] [int] NOT NULL,
	[id_nivel] [int] NOT NULL,
	[numerozona] [int] NOT NULL,
	[nombre] [char](100) NOT NULL,
	[clave] [char](10) NOT NULL,
	[id_sostenimiento] [int] NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [char](10) NULL,
	[fecha_actualizacion] [datetime] NOT NULL,
	[id_cctnt] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_region] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Zona]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Zona](
	[id_zona] [int] NOT NULL,
	[id_tipoeducacion] [int] NULL,
	[id_nivel] [int] NULL,
	[numerozona] [int] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[clave] [varchar](10) NOT NULL,
	[id_sostenimiento] [int] NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NULL,
	[fecha_actualizacion] [datetime] NOT NULL,
	[Id_CCTNT] [int] NULL,
	[Id_CentroTrabajo] [int] NULL,
	[Id_Region] [int] NULL,
 CONSTRAINT [PK_Zona] PRIMARY KEY NONCLUSTERED 
(
	[id_zona] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuariosoax]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuariosoax](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](60) NOT NULL,
	[status] [char](1) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](60) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombersubn] [char](90) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuarioshidalver]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuarioshidalver](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](60) NOT NULL,
	[status] [char](1) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](60) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombersubn] [char](90) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsuariosEstadistica9112011Inicio]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UsuariosEstadistica9112011Inicio](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](65) NOT NULL,
	[status] [char](1) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](45) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsuariosEstadistica9112011Fin]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UsuariosEstadistica9112011Fin](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](65) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](50) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](55) NOT NULL,
	[status] [char](10) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsuariosEstadistica9112010]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UsuariosEstadistica9112010](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](3) NOT NULL,
	[director] [char](59) NOT NULL,
	[status] [char](12) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[UsuariosEstadistica911]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[UsuariosEstadistica911](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](60) NOT NULL,
	[status] [char](1) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](60) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuariosedomex]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuariosedomex](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](60) NOT NULL,
	[status] [char](1) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](60) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombersubn] [char](90) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuarioscomple]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuarioscomple](
	[entidad] [char](2) NOT NULL,
	[Clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](60) NOT NULL,
	[status] [char](1) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](60) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[usuariohidalgo]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[usuariohidalgo](
	[entidad] [char](2) NOT NULL,
	[Clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](60) NOT NULL,
	[status] [char](1) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](65) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario_Perfil]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario_Perfil](
	[Usu_Id_Usuario] [int] NOT NULL,
	[Id_Perfil] [smallint] NOT NULL,
	[Id_CicloEscolar] [smallint] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario_Pefil_Educacion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario_Pefil_Educacion](
	[Id_Perfil_Educacion] [smallint] NOT NULL,
	[Id_NivelTrabajo] [tinyint] NOT NULL,
	[Usu_Id_Usuario] [int] NOT NULL,
	[Bit_Activo] [bit] NULL,
	[Id_Usuario] [int] NULL,
	[Fecha_Actualizacion] [smalldatetime] NULL,
	[Id_CicloEscolar] [smallint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario_Accion_Denegada]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario_Accion_Denegada](
	[Id_Sistema] [tinyint] NOT NULL,
	[Id_Modulo] [tinyint] NOT NULL,
	[Id_Opcion] [tinyint] NOT NULL,
	[Id_Accion] [tinyint] NOT NULL,
	[Usu_Id_Usuario] [int] NOT NULL,
	[Id_CicloEscolar] [smallint] NOT NULL,
	[Bit_Autorizacion] [bit] NULL,
	[Bit_Activo] [bit] NULL,
	[Fecha_Actualizacion] [smalldatetime] NULL,
	[Id_Usuario] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Usuario_Accion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario_Accion](
	[Usu_Id_Usuario] [int] NOT NULL,
	[Id_CicloEscolar] [smallint] NOT NULL,
	[Id_Sistema] [tinyint] NOT NULL,
	[Id_Modulo] [tinyint] NOT NULL,
	[Id_Opcion] [tinyint] NOT NULL,
	[Id_Accion] [tinyint] NOT NULL,
	[Bit_Autorizacion] [bit] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[faltante]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[faltante](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](60) NOT NULL,
	[paginaweb] [char](60) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](20) NOT NULL,
	[nombremod] [char](10) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[renglon] [char](10) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EstatusCT]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstatusCT](
	[id_estatus] [smallint] NOT NULL,
	[nombre] [varchar](20) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EstadoSolicitud]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EstadoSolicitud](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](20) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NULL,
	[Fecha_Actualizacion] [datetime] NOT NULL,
 CONSTRAINT [PK_EstadoSolicitud] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Entidad]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Entidad](
	[Id_Pais] [smallint] NOT NULL,
	[Id_Entidad] [smallint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](6) NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Entidad] PRIMARY KEY NONCLUSTERED 
(
	[Id_Pais] ASC,
	[Id_Entidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EncuestasPreguntas]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EncuestasPreguntas](
	[Id_Pregunta] [smallint] NOT NULL,
	[Id_Encuesta] [smallint] NOT NULL,
	[Pregunta] [varchar](150) NOT NULL,
	[BitActivo] [bit] NOT NULL,
	[BitEsperaRespuesta] [bit] NOT NULL,
	[BitPregObligatoria] [bit] NOT NULL,
	[TipoDato] [varchar](2) NOT NULL,
	[Longitud] [smallint] NOT NULL,
	[DependeDePregunta] [smallint] NOT NULL,
	[EsperaValor] [smallint] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EncuestasParametrosDeAlcance]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EncuestasParametrosDeAlcance](
	[Id_Encuesta] [smallint] NOT NULL,
	[Id_Nivel] [smallint] NOT NULL,
	[Id_Subnivel] [smallint] NOT NULL,
	[Id_Sostenimiento] [smallint] NOT NULL,
	[Id_Municipio] [smallint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EncuestasOpcionesPreguntas]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EncuestasOpcionesPreguntas](
	[Id_Encuesta] [smallint] NOT NULL,
	[Id_Pregunta] [smallint] NOT NULL,
	[Id_Opcion] [smallint] NOT NULL,
	[Texto] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[EncuestasEscuelasMuestra]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[EncuestasEscuelasMuestra](
	[Id_CentroTrabajo] [smallint] NOT NULL,
	[Id_turno] [smallint] NOT NULL,
	[Id_Encuesta] [smallint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[EncuestasDatosGenerales]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[EncuestasDatosGenerales](
	[Id_Encuesta] [smallint] NOT NULL,
	[Nombre] [varchar](70) NOT NULL,
	[Objetivo] [varchar](200) NOT NULL,
	[Solicitante] [varchar](60) NOT NULL,
	[Periodicidad] [varchar](2) NOT NULL,
	[TipoEncuesta] [varchar](1) NOT NULL,
	[Representatividad] [varchar](10) NOT NULL,
	[FechaInicio] [datetime] NOT NULL,
	[FechaFin] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Encuestas]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Encuestas](
	[Id_CentroTrabajo] [smallint] NOT NULL,
	[Id_turno] [smallint] NOT NULL,
	[Id_Encuesta] [smallint] NOT NULL,
	[Fecha] [datetime] NOT NULL,
	[Id_Pregunta] [smallint] NOT NULL,
	[Respuesta] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TurnoCriterio]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TurnoCriterio](
	[id_turnocriterio] [smallint] NOT NULL,
	[id_turno] [smallint] NOT NULL,
	[nombre] [varchar](50) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Turno]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Turno](
	[Id_Turno] [smallint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](10) NOT NULL,
	[Hora_Inicio] [smalldatetime] NOT NULL,
	[Hora_Fin] [smalldatetime] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Turno] PRIMARY KEY NONCLUSTERED 
(
	[Id_Turno] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoTelefono]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoTelefono](
	[Id_TipoTelefono] [smallint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](10) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoEducacion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoEducacion](
	[id_tipoeducacion] [smallint] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [smallint] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoCicloEscolar]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoCicloEscolar](
	[Id_TipoCiclo] [smallint] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Descripcion] [varchar](100) NULL,
	[Bit_Activo] [bit] NULL,
	[Fecha_Actualizacion] [smalldatetime] NULL,
	[id_Usuario] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TipoCentroTrabajo]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TipoCentroTrabajo](
	[id_tipoct] [char](1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TelefonoCentroTrabajo]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TelefonoCentroTrabajo](
	[Id_CentroTrabajo] [smallint] NOT NULL,
	[Id_TelefonoCentroTrabajo] [smallint] NOT NULL,
	[Id_TipoTelefono] [smallint] NOT NULL,
	[Numero] [varchar](20) NOT NULL,
	[Extension] [varchar](10) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subnivel]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subnivel](
	[id_tipoeducacion] [smallint] NOT NULL,
	[id_nivel] [smallint] NOT NULL,
	[id_subnivel] [smallint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [smallint] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Subnivel] PRIMARY KEY NONCLUSTERED 
(
	[id_nivel] ASC,
	[id_subnivel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Subcontrol]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Subcontrol](
	[id_control] [smallint] NOT NULL,
	[id_subcontrol] [smallint] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [smallint] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sostenimiento]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sostenimiento](
	[Id_Sostenimiento] [tinyint] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[BitFederal] [bit] NOT NULL,
	[BitEstatal] [bit] NOT NULL,
	[BitParticular] [bit] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Sostenimiento] PRIMARY KEY NONCLUSTERED 
(
	[Id_Sostenimiento] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sistema_Puerto]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sistema_Puerto](
	[Id_Sistema] [tinyint] NOT NULL,
	[Puerto] [varchar](20) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sistema]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sistema](
	[Id_Sistema] [tinyint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](10) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Fecha_Inicio] [smalldatetime] NULL,
	[Fecha_Fin] [smalldatetime] NULL,
	[Carpeta_Sistema] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SessionStat]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[SessionStat](
	[id] [int] NOT NULL,
	[fechaMedia] [smalldatetime] NULL,
	[cantidad] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[SESSIONES]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SESSIONES](
	[ID_SESSION] [char](90) NOT NULL,
	[FECHA] [datetime] NULL,
	[DATOS] [varbinary](8000) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Servicio]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Servicio](
	[id_servicio] [smallint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[clave] [varchar](3) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [smallint] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Sector]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Sector](
	[id_sector] [smallint] NOT NULL,
	[id_tipoeducacion] [smallint] NULL,
	[id_nivel] [smallint] NULL,
	[numerosector] [smallint] NULL,
	[nombre] [varchar](100) NULL,
	[clave] [varchar](10) NOT NULL,
	[id_sostenimiento] [tinyint] NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Region]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Region](
	[Id_Pais] [int] NOT NULL,
	[Id_Entidad] [int] NOT NULL,
	[Id_Region] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Clave] [varchar](10) NULL,
	[Letra_Folio] [char](1) NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [datetime] NOT NULL,
	[Id_CCTNT] [int] NULL,
	[Id_CentroTrabajo] [int] NULL,
 CONSTRAINT [PK_Region] PRIMARY KEY NONCLUSTERED 
(
	[Id_Region] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY],
 CONSTRAINT [UK_Region] UNIQUE NONCLUSTERED 
(
	[Id_Entidad] ASC,
	[Id_Region] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PuntoCardinal]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[PuntoCardinal](
	[id_puntocardinal] [smallint] NOT NULL,
	[nombre] [varchar](30) NOT NULL,
	[clave] [varchar](2) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[PersonalCP]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PersonalCP](
	[Fecha_Inicio] [smalldatetime] NOT NULL,
	[Fecha_Fin] [smalldatetime] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Id_ClavePresupuestal] [int] NULL,
	[Id_PersonalOld] [int] NOT NULL,
	[Id_Personal] [int] NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Personal]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Personal](
	[Id_Personal] [int] NOT NULL,
	[Id_CCTNT] [smallint] NOT NULL,
	[Id_Persona] [int] NOT NULL,
	[Fecha_Inicio] [smalldatetime] NOT NULL,
	[Fecha_Fin] [smalldatetime] NULL,
	[Id_PersonalFuncion] [int] NULL,
	[bit_DirectorEncargado] [bit] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Persona3]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Persona3](
	[Id_Persona] [int] NOT NULL,
	[Id_Pais] [smallint] NOT NULL,
	[Id_Entidad] [smallint] NOT NULL,
	[Curp] [varchar](20) NULL,
	[Crip] [varchar](24) NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Apellido_Paterno] [varchar](50) NOT NULL,
	[Apellido_Materno] [varchar](50) NOT NULL,
	[Sexo] [char](1) NOT NULL,
	[Fecha_Nacimiento] [datetime] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Id_Padre_Persona] [int] NULL,
	[Id_Madre_Persona] [int] NULL,
	[Bit_Buscado] [bit] NULL,
	[Bit_Encontrado] [bit] NULL,
 CONSTRAINT [PK_Persona_1] PRIMARY KEY CLUSTERED 
(
	[Id_Persona] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Persona]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Persona](
	[Id_Persona] [int] NOT NULL,
	[Id_Pais] [smallint] NOT NULL,
	[Id_Entidad] [smallint] NOT NULL,
	[Curp] [varchar](20) NULL,
	[Crip] [varchar](24) NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Apellido_Paterno] [varchar](50) NOT NULL,
	[Apellido_Materno] [varchar](50) NOT NULL,
	[Sexo] [char](1) NOT NULL,
	[Fecha_Nacimiento] [datetime] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Id_Padre_Persona] [int] NULL,
	[Id_Madre_Persona] [int] NULL,
	[Bit_Buscado] [bit] NULL,
	[Bit_Encontrado] [bit] NULL,
 CONSTRAINT [PK_Persona] PRIMARY KEY NONCLUSTERED 
(
	[Id_Persona] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Perfil_LugarTrabajo]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfil_LugarTrabajo](
	[Id_Perfil] [smallint] NOT NULL,
	[Cnsc_PerfilLugarTrabajo] [tinyint] NOT NULL,
	[Id_NivelTrabajo] [tinyint] NOT NULL,
	[Id_Pais] [smallint] NOT NULL,
	[Id_Entidad] [smallint] NOT NULL,
	[Id_Region] [smallint] NOT NULL,
	[Id_Zona] [smallint] NOT NULL,
	[Id_CentroTrabajo] [smallint] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Perfil_Educacion_Sostenimiento]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfil_Educacion_Sostenimiento](
	[Id_Sostenimiento] [tinyint] NOT NULL,
	[Id_Perfil_Educacion] [smallint] NOT NULL,
	[Id_NivelTrabajo] [tinyint] NOT NULL,
	[Bit_Activo] [bit] NULL,
	[Id_Usuario] [int] NULL,
	[Fecha_Actualizacion] [smalldatetime] NULL,
	[Id_Sistema] [tinyint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Perfil_Educacion_Nivel]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfil_Educacion_Nivel](
	[Id_Perfil_Educacion] [smallint] NOT NULL,
	[Id_NivelTrabajo] [tinyint] NOT NULL,
	[Bit_Activo] [bit] NULL,
	[Id_Usuario] [int] NULL,
	[Fecha_Actualizacion] [smalldatetime] NULL,
	[Id_Sistema] [tinyint] NOT NULL,
	[id_tipoeducacion] [smallint] NOT NULL,
	[id_nivel] [smallint] NOT NULL,
	[id_subnivel] [smallint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Perfil_Educacion_Accion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfil_Educacion_Accion](
	[Id_Sistema] [tinyint] NOT NULL,
	[Id_Modulo] [tinyint] NOT NULL,
	[Id_Opcion] [tinyint] NOT NULL,
	[Id_Accion] [tinyint] NOT NULL,
	[Id_Perfil_Educacion] [smallint] NOT NULL,
	[Bit_Autorizacion] [bit] NULL,
	[Bit_Activo] [bit] NULL,
	[Id_Usuario] [int] NULL,
	[Fecha_Actualizacion] [smalldatetime] NULL,
	[Id_NivelTrabajo] [tinyint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Perfil_Educacion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Perfil_Educacion](
	[Id_Perfil_Educacion] [smallint] NOT NULL,
	[Nombre] [varchar](50) NULL,
	[Abreviatura] [varchar](10) NULL,
	[Bit_Activo] [bit] NULL,
	[Id_Usuario] [int] NULL,
	[Fecha_Actualizacion] [smalldatetime] NULL,
	[Id_NivelTrabajo] [tinyint] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Perfil_Accion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Perfil_Accion](
	[Id_Perfil] [smallint] NOT NULL,
	[Id_Sistema] [tinyint] NOT NULL,
	[Id_Modulo] [tinyint] NOT NULL,
	[Id_Opcion] [tinyint] NOT NULL,
	[Id_Accion] [tinyint] NOT NULL,
	[Bit_Autorizadion] [bit] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Perfil]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Perfil](
	[Id_Perfil] [smallint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](10) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ParametroSistema]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ParametroSistema](
	[Id_Parametro] [int] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Descripcion] [varchar](100) NOT NULL,
	[Id_Tipo] [int] NOT NULL,
	[Valor] [varchar](150) NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [datetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Pais]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Pais](
	[Id_Pais] [smallint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Nacionalidad] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](3) NULL,
	[Id_RegionContinente] [int] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Opcion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Opcion](
	[Id_Sistema] [tinyint] NOT NULL,
	[Id_Modulo] [tinyint] NOT NULL,
	[Id_Opcion] [tinyint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](10) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Fecha_Inicio] [smalldatetime] NULL,
	[Fecha_Fin] [smalldatetime] NULL,
	[Carpeta_Opcion] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NivelTrabajo]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NivelTrabajo](
	[Id_NivelTrabajo] [tinyint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](10) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NivelEducacionBasica]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[NivelEducacionBasica](
	[Id_NivelEducacion] [tinyint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Letra_Folio] [char](1) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Nivel]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Nivel](
	[id_tipoeducacion] [smallint] NOT NULL,
	[id_nivel] [smallint] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[letra_folio] [char](1) NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [smallint] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Nivel] PRIMARY KEY NONCLUSTERED 
(
	[id_nivel] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Municipio]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Municipio](
	[Id_Pais] [smallint] NOT NULL,
	[Id_Entidad] [smallint] NOT NULL,
	[Id_Municipio] [smallint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Zona_Economica] [varchar](3) NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Municipio] PRIMARY KEY NONCLUSTERED 
(
	[Id_Pais] ASC,
	[Id_Entidad] ASC,
	[Id_Municipio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Modulo]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Modulo](
	[Id_Sistema] [tinyint] NOT NULL,
	[Id_Modulo] [tinyint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](10) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Fecha_Inicio] [smalldatetime] NULL,
	[Fecha_Fin] [smalldatetime] NULL,
	[Carpeta_Modulo] [varchar](255) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Modalidad]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Modalidad](
	[id_modalidad] [smallint] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Localidad]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Localidad](
	[Id_Pais] [int] NOT NULL,
	[Id_Entidad] [int] NOT NULL,
	[Id_Municipio] [int] NOT NULL,
	[Id_Localidad] [int] NOT NULL,
	[Id_CategoriaLocalidad] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Codigo_Postal] [varchar](5) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Localidad] PRIMARY KEY NONCLUSTERED 
(
	[Id_Pais] ASC,
	[Id_Entidad] ASC,
	[Id_Municipio] ASC,
	[Id_Localidad] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Inmueble]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Inmueble](
	[Id_Inmueble] [int] NOT NULL,
	[Id_Domicilio] [int] NULL,
	[Clave] [varchar](8) NULL,
	[Fecha_Alta] [datetime] NULL,
	[AGEB] [varchar](20) NULL,
	[Carta_Topografica] [varchar](20) NULL,
	[Clave_Cartografica] [varchar](20) NULL,
	[Bit_Provisional] [bit] NULL,
	[Nombre] [varchar](50) NULL,
	[Longitud] [int] NULL,
	[Latitud] [int] NULL,
	[Altitud] [int] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Id_Ct_Responsable] [int] NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Inmueble] PRIMARY KEY NONCLUSTERED 
(
	[Id_Inmueble] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Incorporacion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Incorporacion](
	[id_incorporacion] [smallint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[identificador]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[identificador](
	[id_identificador] [smallint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[clave] [varchar](3) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [smallint] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[hidalgoveracruzcct]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hidalgoveracruzcct](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](50) NOT NULL,
	[paginaweb] [char](50) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](7) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nombredepo] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](1) NOT NULL,
	[nombremod] [char](15) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](40) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](80) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[hidalgocontraseñas]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[hidalgocontraseñas](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](60) NOT NULL,
	[status] [char](1) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](60) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CentroTrabajo_Incorporacion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CentroTrabajo_Incorporacion](
	[Id_CentroTrabajo] [smallint] NOT NULL,
	[Numero_Incorporacion] [varchar](25) NULL,
	[Id_Incorporacion] [smallint] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CentroTrabajo_Email]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CentroTrabajo_Email](
	[id_centrotrabajo] [smallint] NOT NULL,
	[email] [varchar](100) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CentroTrabajo]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CentroTrabajo](
	[Id_CentroTrabajo] [int] NOT NULL,
	[Id_Pais] [int] NOT NULL,
	[Id_Entidad] [int] NOT NULL,
	[Id_Region] [int] NOT NULL,
	[Id_Zona] [int] NOT NULL,
	[Id_Sostenimiento] [int] NOT NULL,
	[Clave] [varchar](10) NOT NULL,
	[Turno3d] [varchar](3) NULL,
	[BitProvisional] [bit] NOT NULL,
	[Id_Inmueble] [int] NULL,
	[Id_Domicilio] [int] NULL,
	[Fecha_Fundacion] [datetime] NULL,
	[Fecha_Alta] [datetime] NULL,
	[Fecha_Clausura] [datetime] NULL,
	[Id_MotivoBajaCentroTrabajo] [int] NULL,
	[Fecha_Reapertura] [datetime] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [datetime] NOT NULL,
	[Fecha_Cambio] [datetime] NULL,
	[Nombre] [varchar](100) NULL,
	[id_tipoct] [char](1) NOT NULL,
	[id_tipoeducacion] [int] NOT NULL,
	[id_nivel] [int] NOT NULL,
	[id_subnivel] [int] NOT NULL,
	[id_control] [int] NOT NULL,
	[id_subcontrol] [int] NOT NULL,
	[id_dependencianormativa] [int] NULL,
	[id_dependenciaadministrativa] [int] NULL,
	[id_servicio] [int] NOT NULL,
	[id_sector] [int] NULL,
	[id_educacionfisica] [int] NOT NULL,
	[id_estatus] [int] NOT NULL,
	[id_almacen] [int] NOT NULL,
	[id_puntocardinal] [int] NOT NULL,
	[numero_incorporacion] [varchar](100) NULL,
	[folio] [varchar](100) NULL,
	[id_dependenciaoperativa] [int] NULL,
	[observaciones] [varchar](1500) NULL,
	[id_incorporacion] [int] NULL,
	[fechasol] [datetime] NULL,
	[id_claveinstitucional] [int] NULL,
	[Id_NivelEducacion] [int] NULL,
	[Id_ClaveAgrupador] [int] NOT NULL,
	[id_turno] [int] NULL,
	[Valid_Datos] [numeric](2, 0) NOT NULL,
 CONSTRAINT [PK_CentroTrabajo] PRIMARY KEY NONCLUSTERED 
(
	[Id_CentroTrabajo] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Cctstemp]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Cctstemp](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](60) NOT NULL,
	[paginaweb] [char](60) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](20) NOT NULL,
	[nombremod] [char](10) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cctsoaxaca]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cctsoaxaca](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](50) NOT NULL,
	[paginaweb] [char](50) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](7) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nombredepa] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nombredepo] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](1) NOT NULL,
	[nombremod] [char](15) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](40) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](80) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ccts2011INICIO]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ccts2011INICIO](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](60) NOT NULL,
	[paginaweb] [char](60) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](20) NOT NULL,
	[nombremod] [char](10) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[renglon] [char](10) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ccts2011FIn]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ccts2011FIn](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](60) NOT NULL,
	[paginaweb] [char](60) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](20) NOT NULL,
	[nombremod] [char](10) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[claveinm] [char](8) NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ccts2010]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ccts2010](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](50) NOT NULL,
	[paginaweb] [char](50) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](7) NOT NULL,
	[latitud] [char](6) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](1) NOT NULL,
	[nombremod] [char](25) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](12) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[claveinm] [char](8) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ccts2]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ccts2](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](60) NOT NULL,
	[paginaweb] [char](60) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](20) NOT NULL,
	[nombremod] [char](10) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[renglon] [char](10) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Ccts]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Ccts](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](60) NOT NULL,
	[paginaweb] [char](60) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](20) NOT NULL,
	[nombremod] [char](10) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cctfhidalgo]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cctfhidalgo](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](60) NOT NULL,
	[paginaweb] [char](60) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](1) NOT NULL,
	[nombremod] [char](30) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CCTF20091]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CCTF20091](
	[CLAVECCT] [varchar](10) NULL,
	[TIPO_CT_ID] [varchar](1) NULL,
	[ENTIDAD] [smallint] NULL,
	[CLASIFICA] [varchar](1) NULL,
	[IDENTIFICA] [varchar](2) NULL,
	[NUMPROG] [smallint] NULL,
	[ELEVER] [varchar](1) NULL,
	[NOMBRECT] [varchar](102) NULL,
	[DOMICILIO] [varchar](102) NULL,
	[ENTRECALLE] [varchar](42) NULL,
	[YCALLE] [varchar](40) NULL,
	[COLONIA] [varchar](max) NULL,
	[NOMBRECOL] [varchar](62) NULL,
	[LOCALIDAD] [smallint] NULL,
	[NOMBRELOC] [varchar](45) NULL,
	[MUNICIPIO] [smallint] NULL,
	[NOMBREMUN] [varchar](25) NULL,
	[NOMBREENT] [varchar](14) NULL,
	[CODPOST] [smallint] NULL,
	[TELEFONO] [bigint] NULL,
	[TELEXTEN] [varchar](max) NULL,
	[FAX] [bigint] NULL,
	[FAXEXTEN] [varchar](max) NULL,
	[CORREOELE] [varchar](40) NULL,
	[PAGINAWEB] [varchar](max) NULL,
	[TURNO] [smallint] NULL,
	[NOMBRETUR] [varchar](21) NULL,
	[SECTOR] [varchar](max) NULL,
	[CCT_SECTOR] [varchar](max) NULL,
	[ZONAESCOLA] [smallint] NULL,
	[CCT_ZONA] [varchar](10) NULL,
	[SUPEDUCFIS] [varchar](max) NULL,
	[CCT_EDUFIS] [varchar](max) NULL,
	[SERREG] [smallint] NULL,
	[CCT_SERREG] [varchar](10) NULL,
	[AGEB] [smallint] NULL,
	[CLAVECART] [varchar](6) NULL,
	[LONGITUD] [int] NULL,
	[LATITUD] [int] NULL,
	[ALTITUD] [smallint] NULL,
	[CATPOB] [varchar](max) NULL,
	[DEPADMVA] [smallint] NULL,
	[DEPNORMTVA] [varchar](2) NULL,
	[NOMBREDEP] [varchar](max) NULL,
	[TIPO] [smallint] NULL,
	[NOMBRETIPO] [varchar](24) NULL,
	[NIVEL] [smallint] NULL,
	[NOMBRENIV] [varchar](19) NULL,
	[SUBNIVEL] [smallint] NULL,
	[NOMBRESUBN] [varchar](33) NULL,
	[SER] [varchar](max) NULL,
	[SERVICIO] [smallint] NULL,
	[NOMBRESER] [varchar](102) NULL,
	[MODALIDAD] [varchar](max) NULL,
	[CONTROL] [smallint] NULL,
	[NOMBRECONT] [varchar](7) NULL,
	[SUBCONTROL] [smallint] NULL,
	[NOMBRESUBC] [varchar](7) NULL,
	[SUBCONTRO2] [smallint] NULL,
	[NOMSUBCON2] [varchar](19) NULL,
	[SOSTENIMIE] [smallint] NULL,
	[NOMBRESOS] [varchar](49) NULL,
	[INCORPORAC] [varchar](max) NULL,
	[NUMINCORPO] [varchar](max) NULL,
	[FECHAINCOR] [varchar](max) NULL,
	[DIRECTOR] [varchar](42) NULL,
	[CLAVEINSTI] [varchar](10) NULL,
	[CCT_ALMACE] [varchar](10) NULL,
	[ALMACEN] [smallint] NULL,
	[FECHAALTA] [int] NULL,
	[FECHACAMBI] [int] NULL,
	[FECHACLAUS] [int] NULL,
	[FECHAREAPE] [int] NULL,
	[FECHAACTUA] [int] NULL,
	[FECHASOL] [int] NULL,
	[FECHAFUNDA] [int] NULL,
	[MOTIVO] [smallint] NULL,
	[CLAVEINM] [varchar](max) NULL,
	[VIGENCIA] [varchar](max) NULL,
	[STATUS] [smallint] NULL,
	[RENGLON] [smallint] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cctedomex]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cctedomex](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](50) NOT NULL,
	[paginaweb] [char](50) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](7) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nombredepa] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nombredepo] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](1) NOT NULL,
	[nombremod] [char](15) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](40) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](80) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cctCriteria]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cctCriteria](
	[id_regla] [smallint] NULL,
	[tipo_ct] [char](1) NULL,
	[clasificador] [char](1) NULL,
	[id_identificador] [smallint] NULL,
	[identificador] [varchar](2) NULL,
	[id_dependencianormativa] [smallint] NULL,
	[dependencia_normativa] [varchar](2) NULL,
	[id_dependenciaoperativa] [smallint] NULL,
	[dependencia_operativa] [varchar](2) NULL,
	[id_servicio] [smallint] NULL,
	[servicio] [varchar](3) NULL,
	[sostenimiento] [smallint] NULL,
	[nivel] [smallint] NULL,
	[modalidad] [smallint] NULL,
	[tipo] [smallint] NULL,
	[control] [smallint] NULL,
	[subcontrol] [smallint] NULL,
	[subnivel] [int] NULL,
	[agrupacion] [varchar](20) NULL,
	[status] [smallint] NULL,
	[jefasect] [smallint] NULL,
	[zonaescola] [smallint] NULL,
	[educfisica] [smallint] NULL,
	[serreg] [smallint] NULL,
	[almacen] [smallint] NULL,
	[zonatipo] [varchar](3) NULL,
	[edufistipo] [varchar](3) NULL,
	[serregtipo] [varchar](3) NULL,
	[almatipo] [varchar](5) NULL,
	[sectipo] [varchar](3) NULL,
	[inicio] [varchar](10) NULL,
	[fin] [varchar](10) NULL,
	[renglon] [varchar](8) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CCT]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CCT](
	[CLAVECCT] [varchar](100) NULL,
	[TIPO_CT_ID] [varchar](100) NULL,
	[ENTIDAD] [varchar](100) NULL,
	[CLASIFICA] [varchar](100) NULL,
	[IDENTIFICA] [varchar](100) NULL,
	[NUMPROG] [varchar](100) NULL,
	[ELEVER] [varchar](100) NULL,
	[NOMBRECT] [varchar](100) NULL,
	[DOMICILIO] [varchar](100) NULL,
	[ENTRECALLE] [varchar](100) NULL,
	[YCALLE] [varchar](100) NULL,
	[COLONIA] [varchar](100) NULL,
	[NOMBRECOL] [varchar](100) NULL,
	[LOCALIDAD] [varchar](100) NULL,
	[NOMBRELOC] [varchar](100) NULL,
	[MUNICIPIO] [varchar](100) NULL,
	[NOMBREMUN] [varchar](100) NULL,
	[NOMBREENT] [varchar](100) NULL,
	[CODPOST] [varchar](100) NULL,
	[TELEFONO] [varchar](100) NULL,
	[TELEXTEN] [varchar](100) NULL,
	[FAX] [varchar](100) NULL,
	[FAXEXTEN] [varchar](100) NULL,
	[CORREOELE] [varchar](100) NULL,
	[PAGINAWEB] [varchar](100) NULL,
	[TURNO] [varchar](100) NULL,
	[NOMBRETUR] [varchar](100) NULL,
	[SECTOR] [varchar](100) NULL,
	[CCT_SECTOR] [varchar](100) NULL,
	[ZONAESCOLA] [varchar](100) NULL,
	[CCT_ZONA] [varchar](100) NULL,
	[SUPEDUCFIS] [varchar](100) NULL,
	[CCT_EDUFIS] [varchar](50) NULL,
	[SERREG] [varchar](50) NULL,
	[CCT_SERREG] [varchar](50) NULL,
	[AGEB] [varchar](50) NULL,
	[CLAVECART] [varchar](50) NULL,
	[LONGITUD] [varchar](50) NULL,
	[LATITUD] [varchar](50) NULL,
	[ALTITUD] [varchar](50) NULL,
	[CATPOB] [varchar](50) NULL,
	[DEPADMVA] [varchar](50) NULL,
	[DEPNORMTVA] [varchar](50) NULL,
	[NOMBREDEP] [varchar](50) NULL,
	[TIPO] [varchar](50) NULL,
	[NOMBRETIPO] [varchar](50) NULL,
	[NIVEL] [varchar](50) NULL,
	[NOMBRENIV] [varchar](50) NULL,
	[SUBNIVEL] [varchar](100) NULL,
	[NOMBRESUBN] [varchar](100) NULL,
	[SER] [varchar](100) NULL,
	[SERVICIO] [varchar](100) NULL,
	[NOMBRESER] [varchar](100) NULL,
	[MODALIDAD] [varchar](100) NULL,
	[CONTROL] [varchar](100) NULL,
	[NOMBRECONT] [varchar](100) NULL,
	[SUBCONTROL] [varchar](100) NULL,
	[NOMBRESUBC] [varchar](100) NULL,
	[SUBCONTRO2] [varchar](100) NULL,
	[NOMSUBCON2] [varchar](100) NULL,
	[SOSTENIMIE] [varchar](100) NULL,
	[NOMBRESOS] [varchar](100) NULL,
	[INCORPORAC] [varchar](100) NULL,
	[NUMINCORPO] [varchar](100) NULL,
	[FECHAINCOR] [varchar](100) NULL,
	[DIRECTOR] [varchar](100) NULL,
	[CLAVEINSTI] [varchar](100) NULL,
	[CCT_ALMACE] [varchar](100) NULL,
	[ALMACEN] [varchar](100) NULL,
	[FECHAALTA] [varchar](100) NULL,
	[FECHACAMBI] [varchar](100) NULL,
	[FECHACLAUS] [varchar](100) NULL,
	[FECHAREAPE] [varchar](100) NULL,
	[FECHAACTUA] [varchar](100) NULL,
	[FECHASOL] [varchar](100) NULL,
	[FECHAFUNDA] [varchar](100) NULL,
	[MOTIVO] [varchar](100) NULL,
	[CLAVEINM] [varchar](100) NULL,
	[VIGENCIA] [varchar](100) NULL,
	[STATUS] [varchar](100) NULL,
	[RENGLON] [varchar](100) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[accion]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[accion](
	[Id_Sistema] [tinyint] NOT NULL,
	[Id_Modulo] [tinyint] NOT NULL,
	[Id_Opcion] [tinyint] NOT NULL,
	[Id_Accion] [tinyint] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Abreviatura] [varchar](10) NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Fecha_Inicio] [smalldatetime] NULL,
	[Fecha_Fin] [smalldatetime] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DirectorCT]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DirectorCT](
	[Id_CCTNT] [smallint] NOT NULL,
	[Bit_DirectorEncargado] [bit] NULL,
	[Id_Personal] [int] NOT NULL,
	[Fecha_Inicio] [smalldatetime] NOT NULL,
	[Fecha_Fin] [smalldatetime] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DependenciaOperativa]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DependenciaOperativa](
	[id_dependenciaoperativa] [smallint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[clave] [varchar](2) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DependenciaNormativa]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DependenciaNormativa](
	[id_dependencianormativa] [smallint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[clave] [varchar](2) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[DependenciaAdministrativa]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[DependenciaAdministrativa](
	[id_dependenciaadministrativa] [smallint] NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[cs_conafe]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[cs_conafe](
	[entidad] [char](2) NOT NULL,
	[clavecct] [char](10) NOT NULL,
	[turno] [char](1) NOT NULL,
	[contrasena] [char](10) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[director] [char](60) NOT NULL,
	[status] [char](10) NOT NULL,
	[nivel] [char](10) NOT NULL,
	[id_persona] [int] NULL,
	[id_centrotrabajo] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Control]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Control](
	[id_control] [smallint] NOT NULL,
	[nombre] [varchar](50) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[conafe]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[conafe](
	[clavecct] [char](10) NOT NULL,
	[tipo_ct_id] [char](1) NOT NULL,
	[entidad] [char](2) NOT NULL,
	[clasifica] [char](1) NOT NULL,
	[identifica] [char](2) NOT NULL,
	[numprog] [char](4) NOT NULL,
	[elever] [char](1) NOT NULL,
	[nombrect] [char](100) NOT NULL,
	[domicilio] [char](100) NOT NULL,
	[entrecalle] [char](40) NOT NULL,
	[ycalle] [char](40) NOT NULL,
	[colonia] [char](4) NOT NULL,
	[nombrecol] [char](60) NOT NULL,
	[localidad] [char](4) NOT NULL,
	[nombreloc] [char](110) NOT NULL,
	[municipio] [char](3) NOT NULL,
	[nombremun] [char](60) NOT NULL,
	[nombreent] [char](60) NOT NULL,
	[codpost] [char](5) NOT NULL,
	[telefono] [char](12) NOT NULL,
	[telexten] [char](5) NOT NULL,
	[fax] [char](12) NOT NULL,
	[faxexten] [char](5) NOT NULL,
	[correoele] [char](60) NOT NULL,
	[paginaweb] [char](60) NOT NULL,
	[turno] [char](3) NOT NULL,
	[nombretur] [char](50) NOT NULL,
	[sector] [char](2) NOT NULL,
	[cct_sector] [char](10) NOT NULL,
	[zonaescola] [char](3) NOT NULL,
	[cct_zona] [char](10) NOT NULL,
	[supeducfis] [char](3) NOT NULL,
	[cct_edufis] [char](10) NOT NULL,
	[serreg] [char](3) NOT NULL,
	[cct_serreg] [char](10) NOT NULL,
	[ageb] [char](5) NOT NULL,
	[clavecart] [char](6) NOT NULL,
	[longitud] [char](14) NOT NULL,
	[latitud] [char](12) NOT NULL,
	[altitud] [char](4) NOT NULL,
	[catpob] [char](1) NOT NULL,
	[ambito] [char](1) NOT NULL,
	[depadmva] [char](1) NOT NULL,
	[nomdepadmv] [char](100) NOT NULL,
	[depoper] [char](2) NOT NULL,
	[nomdepoper] [char](100) NOT NULL,
	[depnormtva] [char](2) NOT NULL,
	[nombredep] [char](100) NOT NULL,
	[tipo] [char](1) NOT NULL,
	[nombretipo] [char](40) NOT NULL,
	[nivel] [char](2) NOT NULL,
	[nombreniv] [char](60) NOT NULL,
	[subnivel] [char](3) NOT NULL,
	[nombresubn] [char](90) NOT NULL,
	[ser] [char](3) NOT NULL,
	[servicio] [char](2) NOT NULL,
	[nombreser] [char](100) NOT NULL,
	[modalidad] [char](20) NOT NULL,
	[nombremod] [char](10) NOT NULL,
	[control] [char](1) NOT NULL,
	[nombrecont] [char](9) NOT NULL,
	[subcontrol] [char](1) NOT NULL,
	[nombresubc] [char](40) NOT NULL,
	[sostenimie] [char](2) NOT NULL,
	[nombresos] [char](100) NOT NULL,
	[incorporac] [char](1) NOT NULL,
	[numincorpo] [char](22) NOT NULL,
	[fechaincor] [char](8) NOT NULL,
	[director] [char](50) NOT NULL,
	[claveinsti] [char](10) NOT NULL,
	[cct_almace] [char](10) NOT NULL,
	[fechaalta] [char](8) NOT NULL,
	[fechacambi] [char](8) NOT NULL,
	[fechaclaus] [char](8) NOT NULL,
	[fechareape] [char](8) NOT NULL,
	[fechaactua] [char](8) NOT NULL,
	[fechasol] [char](8) NOT NULL,
	[fechafunda] [char](8) NOT NULL,
	[motivo] [char](1) NOT NULL,
	[status] [char](1) NOT NULL,
	[almacen] [char](5) NOT NULL,
	[id_Entidad] [int] NULL,
	[id_municipio] [int] NULL,
	[id_localidad] [int] NULL,
	[Id_colonia] [int] NULL,
	[id_domicilio] [int] NULL,
	[id_centrotrabajo] [int] NULL,
	[id_persona] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Colonia]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Colonia](
	[Id_Pais] [int] NOT NULL,
	[Id_Entidad] [int] NOT NULL,
	[Id_Municipio] [int] NOT NULL,
	[Id_Colonia] [int] NOT NULL,
	[Nombre] [varchar](100) NOT NULL,
	[Codigo_Postal] [varchar](5) NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [datetime] NOT NULL,
	[Id_Localidad] [int] NOT NULL,
 CONSTRAINT [PK_Colonia] PRIMARY KEY NONCLUSTERED 
(
	[Id_Pais] ASC,
	[Id_Entidad] ASC,
	[Id_Municipio] ASC,
	[Id_Colonia] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ClavePresupuestal]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ClavePresupuestal](
	[Id_ClavePresupuestal] [int] NOT NULL,
	[Id_Persona] [int] NOT NULL,
	[ClaveA] [varchar](30) NOT NULL,
	[ClaveB] [varchar](30) NULL,
	[Id_Sostenimiento] [tinyint] NOT NULL,
	[Id_Estado] [smallint] NULL,
	[Id_Usuario] [int] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Id_Puesto] [int] NULL,
	[Fecha_Fin] [smalldatetime] NULL,
	[NumeroHoras] [smallint] NULL,
	[cct] [varchar](10) NULL,
	[hasta] [varchar](8) NULL,
	[Id_Categoria] [int] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Clasificador]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Clasificador](
	[id_clasificador] [char](1) NOT NULL,
	[nombre] [varchar](100) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [smallint] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CicloEscolarActual]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CicloEscolarActual](
	[Id_CicloEscolarAnterior] [smallint] NOT NULL,
	[Id_CicloEscolarActual] [smallint] NOT NULL,
	[Id_CicloEscolarSiguiente] [smallint] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Id_TipoCiclo] [smallint] NOT NULL
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[CicloEscolar]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CicloEscolar](
	[Id_CicloEscolar] [smallint] NOT NULL,
	[Id_TipoCiclo] [smallint] NOT NULL,
	[Fecha_Inicio] [smalldatetime] NOT NULL,
	[Fecha_Fin] [smalldatetime] NOT NULL,
	[Nombre] [varchar](50) NOT NULL,
	[Bit_Activo] [bit] NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [datetime] NOT NULL,
	[id_CicloEscolarAnterior] [smallint] NULL,
	[id_CicloEscolarSiguiente] [smallint] NULL,
	[Bit_CicloActual] [bit] NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CentroTrabajo_SolicitanteDeAlta]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CentroTrabajo_SolicitanteDeAlta](
	[Id_CentroTrabajo] [smallint] NOT NULL,
	[Area] [varchar](60) NULL,
	[Nombre] [varchar](60) NULL,
	[Puesto] [varchar](50) NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CentroTrabajo_PaginaWeb]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CentroTrabajo_PaginaWeb](
	[id_centrotrabajo] [smallint] NOT NULL,
	[pagina_web] [varchar](100) NOT NULL,
	[bit_activo] [bit] NOT NULL,
	[id_usuario] [int] NOT NULL,
	[fecha_actualizacion] [smalldatetime] NOT NULL
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CentroTrabajo_Nivel_Turno]    Script Date: 05/23/2013 16:21:19 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CentroTrabajo_Nivel_Turno](
	[id_CCTNT] [int] NOT NULL,
	[Id_Anterior] [int] NULL,
	[Id_NivelEducacion] [tinyint] NULL,
	[id_turno] [int] NULL,
	[id_CentroTrabajo] [int] NULL,
	[Clave] [varchar](10) NULL,
	[Bit_Cierre] [bit] NULL,
	[Bit_Activo] [bit] NULL,
	[Fecha_Actualizacion] [datetime] NULL,
	[Id_Usuario] [int] NULL,
 CONSTRAINT [PK_CentroTrabajoNivelTurno] PRIMARY KEY NONCLUSTERED 
(
	[id_CCTNT] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GETNOMBREPERSONAEx]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS OFF
GO
SET QUOTED_IDENTIFIER OFF
GO
-- =============================================
-- Author:		<Jose Maria Martinez>
-- Create date: <09 Enero 2009>
-- Description:	<Regresa el nombre de la persona, el ordenNombre = 1 regresa nombre y apellidos, el 2 Apellidos y nombre>
-- =============================================
Create FUNCTION [dbo].[FN_GETNOMBREPERSONAEx]
(@ID_PERSONA int,
@OrdenNombre int )
RETURNS VarChar(70) 
AS
BEGIN
  Declare @result varchar(70)

  if @ordenNombre = 1
     Set @result = (Select ltrim(rtrim(p.Nombre)) +' '+ ltrim(rtrim(p.Apellido_Paterno)) +' '+ ltrim(rtrim(p.Apellido_Materno)) from persona p 
             where 
              p.Id_Persona = @id_Persona)

  if @ordenNombre = 2
     Set @result = (Select ltrim(rtrim(p.Apellido_Paterno)) +' '+ ltrim(rtrim(p.Apellido_Materno)+' '+ltrim(rtrim(p.Nombre))  ) from persona p 
             where 
              p.Id_Persona = @id_Persona)
  
return @result
 
 
END
GO
/****** Object:  Table [dbo].[SolicitudDBF]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SolicitudDBF](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Id_Entidad] [smallint] NOT NULL,
	[Id_Cuestionario] [smallint] NOT NULL,
	[Id_TipoCuestionario] [smallint] NOT NULL,
	[Id_CicloEscolar] [smallint] NOT NULL,
	[FechaInicio] [datetime] NULL,
	[FechaFin] [datetime] NULL,
	[Id_EstadoSolicitud] [int] NULL,
	[Archivo] [varchar](120) NULL,
	[Id_Usuario] [int] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[FechaAlta] [datetime] NOT NULL,
	[FechaActualizacion] [datetime] NOT NULL,
 CONSTRAINT [PK_SolicitudDBF] PRIMARY KEY NONCLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Domicilio]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Domicilio](
	[Id_Domicilio] [int] NOT NULL,
	[Id_Pais] [int] NOT NULL,
	[Id_Entidad] [int] NOT NULL,
	[Id_Municipio] [int] NOT NULL,
	[Id_Localidad] [int] NULL,
	[Id_Colonia] [int] NULL,
	[Calle] [varchar](100) NOT NULL,
	[Numero_Exterior] [int] NULL,
	[Numero_Interior] [varchar](10) NULL,
	[Piso] [int] NULL,
	[Codigo_Postal] [varchar](5) NOT NULL,
	[EntreCalles] [varchar](100) NULL,
	[EntreCalle] [varchar](40) NULL,
	[YCalle] [varchar](50) NULL,
	[CallePosterior] [varchar](40) NULL,
	[X] [varchar](20) NULL,
	[Y] [varchar](20) NULL,
	[Latitud] [varchar](20) NULL,
	[Longitud] [varchar](20) NULL,
	[Altitud] [varchar](8) NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [datetime] NOT NULL,
 CONSTRAINT [PK_Domicilio] PRIMARY KEY NONCLUSTERED 
(
	[Id_Domicilio] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Usuario]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Usuario](
	[Id_Usuario] [int] NOT NULL,
	[Id_Persona] [int] NOT NULL,
	[Login] [varchar](30) NOT NULL,
	[Password] [varchar](30) NOT NULL,
	[Bit_Bloqueado] [bit] NOT NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Bit_Activo2] [bit] NOT NULL,
	[Id_Usuario2] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
 CONSTRAINT [PK_Usuario] PRIMARY KEY NONCLUSTERED 
(
	[Id_Usuario] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GETNOMBRECICLO]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
create FUNCTION [dbo].[FN_GETNOMBRECICLO]
(@id_cicloescolar int)
RETURNs varchar(20)
AS
BEGIN
  return (Select Nombre From CicloEscolar
		  where
		  id_cicloEscolar = @id_cicloEscolar)
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GETIDDIRECTORCCT]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- User Defined Function
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FN_GETIDDIRECTORCCT]
(
  @id_cctnt int
)
RETURNS int
AS
BEGIN
  declare @idPersona int

  set @idPersona = (SELECT TOP 1 
							Persona.Id_persona
				      FROM 
							Persona, 
							Personal, 
							DirectorCT							
					  WHERE
							DirectorCT.Id_Personal = Personal.Id_Personal						
							AND Personal.Id_Persona = Persona.Id_Persona							
							AND Personal.Fecha_Fin IS NULL
							AND Personal.Bit_Activo = 1
							AND Personal.id_cctnt = @id_cctnt
						ORDER BY dbo.Personal.Fecha_Inicio DESC )

  return @idPersona
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GETCICLOACTUAL]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		Jose Maria Martinez
-- Create date: 10/06/2008
-- Description:	Obtienen el ciclo actual escolar
-- =============================================
CREATE FUNCTION [dbo].[FN_GETCICLOACTUAL]
(@id_tipoCiclo int)
RETURNs int
AS
BEGIN
  return (Select id_CicloEscolar From CicloEscolar
		  where
		  id_TipoCiclo = @id_tipoCiclo
          and bit_CicloActual = 1)
END
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GETNOMBREDIRECTORCCTID]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- User Defined Function
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date, ,>
-- Description:	<Description, ,>
-- =============================================
CREATE FUNCTION [dbo].[FN_GETNOMBREDIRECTORCCTID]
(
  @id_cct int
)
RETURNS int
AS
BEGIN
  declare @IDPersonas varchar(40)
  declare @id_cctnt int

  set @id_cctnt = (SELECT   top 1  CentroTrabajo_Nivel_Turno.id_CCTNT
					FROM CentroTrabajo INNER JOIN CentroTrabajo_Nivel_Turno ON 
					CentroTrabajo.Id_CentroTrabajo = CentroTrabajo_Nivel_Turno.id_CentroTrabajo
					WHERE (CentroTrabajo.Id_CentroTrabajo = @id_cct))

  set @IDPersonas = (select dbo.FN_GETIDDIRECTORCCT(@id_cctnt) )

  return @IDPersonas
END
GO
/****** Object:  Table [dbo].[Usuario_LugarTrabajo]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Usuario_LugarTrabajo](
	[Id_Usuario_LT] [int] NOT NULL,
	[Id_Consecutivo] [smallint] NOT NULL,
	[Cnsc_UsuarioLugarTrabajo] [tinyint] NOT NULL,
	[Id_NivelTrabajo] [tinyint] NOT NULL,
	[Id_Pais] [smallint] NOT NULL,
	[Id_Entidad] [smallint] NOT NULL,
	[Id_Region] [smallint] NOT NULL,
	[Id_Zona] [smallint] NOT NULL,
	[Id_CentroTrabajo] [int] NULL,
	[Bit_Activo] [bit] NOT NULL,
	[Id_Usuario] [int] NOT NULL,
	[Fecha_Actualizacion] [smalldatetime] NOT NULL,
	[Id_Sector] [smallint] NOT NULL,
 CONSTRAINT [PK_UsuarioLT] PRIMARY KEY NONCLUSTERED 
(
	[Id_Usuario_LT] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  UserDefinedFunction [dbo].[FN_GETNOMBREDIRECTORCCTID911]    Script Date: 05/23/2013 16:21:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- User Defined Function
-- =============================================
-- Author:		<Santiago García Puente>
-- Create date: <Create Date, ,>
-- Description:	<Devuelve el nombre del director (usuario) para datos de identificacion de 911>
-- =============================================
CREATE FUNCTION [dbo].[FN_GETNOMBREDIRECTORCCTID911] ( @id_cct INT )
RETURNS VARCHAR(200)
AS BEGIN
    DECLARE @Persona VARCHAR(200)
    
    SET @Persona = ( SELECT TOP 1
                            (p.Nombre + ' ' + p.Apellido_Paterno + ' '
                            + p.Apellido_Materno) AS Nombre
                     FROM   dbo.Usuario_LugarTrabajo ult
                            INNER JOIN dbo.Usuario u ON ult.Id_Usuario_LT = u.Id_Usuario
                            INNER JOIN dbo.Persona p ON u.Id_Persona = p.Id_Persona
                     WHERE  ult.Id_CentroTrabajo = @id_cct
                   )    

    RETURN @Persona
   END
GO
/****** Object:  Default [DF_EstadoSolicitud_Fecha]    Script Date: 05/23/2013 16:21:19 ******/
ALTER TABLE [dbo].[EstadoSolicitud] ADD  CONSTRAINT [DF_EstadoSolicitud_Fecha]  DEFAULT (getdate()) FOR [Fecha_Actualizacion]
GO
/****** Object:  Default [DF_Solicitus_FechaAlta]    Script Date: 05/23/2013 16:21:20 ******/
ALTER TABLE [dbo].[SolicitudDBF] ADD  CONSTRAINT [DF_Solicitus_FechaAlta]  DEFAULT (getdate()) FOR [FechaAlta]
GO
/****** Object:  Default [DF_Solicitus_FechaUpdate]    Script Date: 05/23/2013 16:21:20 ******/
ALTER TABLE [dbo].[SolicitudDBF] ADD  CONSTRAINT [DF_Solicitus_FechaUpdate]  DEFAULT (getdate()) FOR [FechaActualizacion]
GO
/****** Object:  ForeignKey [FK_CentroTrabajoNivelTurno_Centrotrabajo]    Script Date: 05/23/2013 16:21:19 ******/
ALTER TABLE [dbo].[CentroTrabajo_Nivel_Turno]  WITH NOCHECK ADD  CONSTRAINT [FK_CentroTrabajoNivelTurno_Centrotrabajo] FOREIGN KEY([id_CentroTrabajo])
REFERENCES [dbo].[CentroTrabajo] ([Id_CentroTrabajo])
GO
ALTER TABLE [dbo].[CentroTrabajo_Nivel_Turno] CHECK CONSTRAINT [FK_CentroTrabajoNivelTurno_Centrotrabajo]
GO
/****** Object:  ForeignKey [FK_SolicitudDBF_EstadoSolicitud]    Script Date: 05/23/2013 16:21:20 ******/
ALTER TABLE [dbo].[SolicitudDBF]  WITH CHECK ADD  CONSTRAINT [FK_SolicitudDBF_EstadoSolicitud] FOREIGN KEY([Id_EstadoSolicitud])
REFERENCES [dbo].[EstadoSolicitud] ([Id])
GO
ALTER TABLE [dbo].[SolicitudDBF] CHECK CONSTRAINT [FK_SolicitudDBF_EstadoSolicitud]
GO
/****** Object:  ForeignKey [FK__49e50986]    Script Date: 05/23/2013 16:21:20 ******/
ALTER TABLE [dbo].[Domicilio]  WITH NOCHECK ADD  CONSTRAINT [FK__49e50986] FOREIGN KEY([Id_Pais], [Id_Entidad], [Id_Municipio], [Id_Localidad])
REFERENCES [dbo].[Localidad] ([Id_Pais], [Id_Entidad], [Id_Municipio], [Id_Localidad])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[Domicilio] CHECK CONSTRAINT [FK__49e50986]
GO
/****** Object:  ForeignKey [FK__dd955e3c]    Script Date: 05/23/2013 16:21:20 ******/
ALTER TABLE [dbo].[Usuario]  WITH NOCHECK ADD  CONSTRAINT [FK__dd955e3c] FOREIGN KEY([Id_Persona])
REFERENCES [dbo].[Persona] ([Id_Persona])
GO
ALTER TABLE [dbo].[Usuario] CHECK CONSTRAINT [FK__dd955e3c]
GO
/****** Object:  ForeignKey [FK_Usuario_UsuarioLT]    Script Date: 05/23/2013 16:21:20 ******/
ALTER TABLE [dbo].[Usuario_LugarTrabajo]  WITH NOCHECK ADD  CONSTRAINT [FK_Usuario_UsuarioLT] FOREIGN KEY([Id_Usuario_LT])
REFERENCES [dbo].[Usuario] ([Id_Usuario])
GO
ALTER TABLE [dbo].[Usuario_LugarTrabajo] CHECK CONSTRAINT [FK_Usuario_UsuarioLT]
GO

﻿Option Strict On
Imports System.IO
Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Web
Imports System.Web.UI.WebControls
Imports System.Web.Mail
Imports System.Drawing


Public Class _Default

    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load


        Dim MySQLConnection As New SqlClient.SqlConnection
        Dim MySQLCommand As New SqlClient.SqlCommand
        Dim MyDataAdapter As New SqlClient.SqlDataAdapter
        Dim MyDataSet As New Data.DataSet
        Dim sqlQuery As New StringBuilder()


        sqlQuery.Append(" select  v.CLAVECCT, v.N_CLAVECCT, v.TURNO, v.N_ENTIDAD, v.MUNICIPIO, v.N_MUNICIPI, v.LOCALIDAD,v.N_LOCALIDA, v.DOMICILIO,v.DEPADMVA, v.DEPNORMTVA, v.ZONAESCOLA, v.SECTOR, v.DIRSERVREG,")
        sqlQuery.Append("v.SOSTENIMIE, v.SERVICIO, v.UNIDADRESP, v.PROGRAMA, v.SUBPROG, v.RENGLON,v.N_RENGLON, v.PERIODO,MOTIVO = case when Tb_Control.Estatus%10=0 then null else Tb_Control.Estatus end, v.DISPON,c.v1, c.v2, c.v3,")
        sqlQuery.Append("c.v4, c.v5, c.v6, c.v7, c.v8, c.v9, c.v10, c.v11, c.v12, c.v13, c.v14, c.v15, c.v16, c.v17, c.v18, C.v19, c.v20, c.v21, c.v22, c.v23, c.v24, c.v25, c.v26, c.v27, c.v28, c.v29, c.v30, c.v31, c.v32,")
        sqlQuery.Append("c.v33, c.v34, c.v35, c.v36, c.v37, c.v38, c.v39, c.v40, c.v41, c.v42, c.v43, c.v44, c.v45, c.v46, c.v47, c.v48, c.v49, c.v50, c.v51, c.v52, c.v53, c.v54, c.v55, c.v56, c.v57, c.v58, c.v59, c.v60, ")
        sqlQuery.Append("c.v61, c.v62, c.v63, c.v64, c.v65, c.v66, c.v67, c.v68, c.v69, c.v70, c.v71, c.v72, c.v73, c.v74, c.v75, c.v76, c.v77, c.v78, c.v79, c.v80, c.v81, c.v82, c.v83, c.v84, c.v85, c.v86, c.v87, c.v88, ")
        sqlQuery.Append(" c.v89, c.v90, c.v91, c.v92, c.v93, c.v94, c.v95, c.v96, c.v97, c.v98, c.v99, c.v100, c.v101, c.v102, c.v103, c.v104, c.v105, c.v106, c.v107, c.v108, c.v109, c.v110, c.v111, c.v112, c.v113, c.v114, ")
        sqlQuery.Append(" c.v115, c.v116, c.v117, c.v118, c.v119, c.v120, c.v121, c.v122, c.v123, c.v124, c.v125, c.v126, c.v127, c.v128, c.v129, c.v130, c.v131, c.v132, c.v133, c.v134, c.v135, c.v136, c.v137, c.v138, ")
        sqlQuery.Append(" c.v139, c.v140, c.v141, c.v142, c.v143, c.v144, c.v145, c.v146, c.v147, c.v148, c.v149, c.v150, c.v151, c.v152, c.v153, c.v154, c.v155, c.v156, c.v157, c.v158, c.v159, c.v160, c.v161, c.v162, ")
        sqlQuery.Append(" c.v163, c.v164, c.v165, c.v166, c.v167, c.v168, c.v169, c.v170, c.v171, c.v172, c.v173, c.v174, c.v175, c.v176, c.v177, c.v178, c.v179, c.v180, c.v181, c.v182, c.v183, c.v184, c.v185, c.v186, ")
        sqlQuery.Append(" c.v187, c.v188, c.v189, c.v190, c.v191, c.v192, c.v193, c.v194, c.v195, c.v196, c.v197, c.v198, c.v199, c.v200, c.v201, c.v202, c.v203, c.v204, c.v205, c.v206, c.v207, c.v208, c.v209, c.v210, ")
        sqlQuery.Append(" c.v211, c.v212, c.v213, c.v214, c.v215, c.v216, c.v217, c.v218, c.v219, c.v220, c.v221, c.v222, c.v223, c.v224, c.v225, c.v226, c.v227, c.v228, c.v229, c.v230, c.v231, c.v232, c.v233, c.v234, ")
        sqlQuery.Append(" c.v235, c.v236, c.v237, c.v238, c.v239 from [SEP_911].[dbo].[Tb_CONTROL], [SEP_911].[dbo].[Vista_DatosDBF] v,[SEP_911].[dbo].[VW_Preescolar_G_F] c where [SEP_911].[dbo].[Tb_CONTROL].Id_CCTNT = v.Id_CCTNT and  ")
        sqlQuery.Append(" [SEP_911].[dbo].[Tb_CONTROL].id_control	= c.id_control and[SEP_911].[dbo].[Tb_CONTROL].id_tipoCuestionario  = 1 and v.CLAVECCT='14DPR1612F'and TURNO =1 order by v.id_CCTNT ")


        Label1.Text = CStr(sqlQuery.Length)



        MySQLConnection.ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings("911SecuritydbConn").ConnectionString
        MySQLConnection.Open()
        MySQLCommand.Connection = MySQLConnection
        MySQLCommand.CommandType = CommandType.Text
        MySQLCommand.CommandText = Label1.Text
        MyDataAdapter.SelectCommand = MySQLCommand
        'MyDataAdapter.Fill(MyDataSet, "Resultados")


        'GridView1.PageIndex = e.NewPageIndex
        'GridView1.DataSource = MyDataSet
        'GridView1.DataMember = "Resultados"
        'GridView1.DataBind()

        MySQLConnection.Close()
    End Sub

End Class